/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.shapeproject;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author user1
 */
public class CircleFrame {

    public static void main(String[] args) {
        final JFrame frame = new JFrame("Circle");
        frame.setSize(350, 350);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblRadius = new JLabel("radius:", JLabel.TRAILING);
        lblRadius.setSize(47, 20);
        lblRadius.setLocation(5, 5);
        lblRadius.setBackground(Color.white);
        lblRadius.setOpaque(true);

        final JTextField txtRadius = new JTextField();
        txtRadius.setSize(50, 20);
        txtRadius.setLocation(60, 5);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);

        final JLabel lblResult = new JLabel("Circle radius = ??? area = ??? perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(350, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.magenta);
        lblResult.setOpaque(true);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strRadius = txtRadius.getText();
                    double radius = Double.parseDouble(strRadius);
                    Circle circle = new Circle(radius);
                    lblResult.setText("Circle radius=" + String.format("%.2f", circle.getRadius()) 
                                    + " area=" + String.format("%.2f", circle.calArea()) 
                                    + " perimeter=" + String.format("%.2f", circle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error please input number", 
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtRadius.setText("");
                    txtRadius.requestFocus();
                }
            }
        });

        frame.add(lblRadius);
        frame.add(txtRadius);
        frame.add(btnCalculate);
        frame.add(lblResult);

        frame.setVisible(true);
    }

}
