/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.shapeproject;

/**
 *
 * @author user1
 */
public class Triangle extends Shape {

    private double base,hight;

    public Triangle(double base, double hight) {
        super("Triangle");
        this.base = base;
        this.hight = hight;
    }

    public double getBase() {
        return base;
    }

    public double getHight() {
        return hight;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public void setHight(double hight) {
        this.hight = hight;
    }

    @Override
    public double calArea() {
        return 0.5*base*hight;
    }

    @Override
    public double calPerimeter() {
        return 3*base;
    }

}
