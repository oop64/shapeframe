/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.shapeproject;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author user1
 */
public class RectangleFrame {

    public static void main(String[] args) {
        final JFrame frame = new JFrame("Rectangle");
        frame.setSize(400, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        //width
        JLabel lblWidth = new JLabel("width:", JLabel.TRAILING);
        lblWidth.setSize(47, 20);
        lblWidth.setLocation(5, 5);
        lblWidth.setBackground(Color.white);
        lblWidth.setOpaque(true);

        final JTextField txtWidth = new JTextField();
        txtWidth.setSize(50, 20);
        txtWidth.setLocation(60, 5);
        
        //hight
        JLabel lblHight = new JLabel("hight:", JLabel.TRAILING);
        lblHight.setSize(47, 20);
        lblHight.setLocation(5, 30);
        lblHight.setBackground(Color.white);
        lblHight.setOpaque(true);

        final JTextField txtHight = new JTextField();
        txtHight.setSize(50, 20);
        txtHight.setLocation(60, 30);
        
        //
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);

        final JLabel lblResult = new JLabel("Rectangle width = ? hight = ? area = ? perimeter = ?");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setLocation(0, 100);
        lblResult.setBackground(Color.green);
        lblResult.setOpaque(true);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strWidth = txtWidth.getText();
                    String strHight = txtHight.getText();
                    double width = Double.parseDouble(strWidth);
                    double hight = Double.parseDouble(strHight);
                    
                    Rectangle rec = new Rectangle(width,hight);
                    lblResult.setText("Rectangle width=" + String.format("%.2f", rec.getWidth())
                                    + "hight=" + String.format("%.2f", rec.getHight())
                                    + " area=" + String.format("%.2f", rec.calArea()) 
                                    + " perimeter=" + String.format("%.2f", rec.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error please input number", 
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtWidth.setText("");
                    txtHight.setText("");
                    txtWidth.requestFocus();
                }
            }
        });
        frame.add(lblWidth);
        frame.add(txtWidth);
        frame.add(lblHight);
        frame.add(txtHight);
        frame.add(btnCalculate);
        frame.add(lblResult);

        frame.setVisible(true);
    }

}
