/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.shapeproject;

/**
 *
 * @author user1
 */
public class Rectangle extends Shape {

    private double width,hight;

    public Rectangle(double width, double hight) {
        super("Rectangle");
        this.width = width;
        this.hight = hight;
    }

    public double getWidth() {
        return width;
    }

    public double getHight() {
        return hight;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setHight(double hight) {
        this.hight = hight;
    }

    @Override
    public double calArea() {
        return hight * width;
    }

    @Override
    public double calPerimeter() {
        return (2*hight) + (2*width);
    }

}
