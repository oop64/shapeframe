/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.shapeproject;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author user1
 */
public class TriangleFrame {

    public static void main(String[] args) {
        final JFrame frame = new JFrame("Triangle");
        frame.setSize(400, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        //base
        JLabel lblBase = new JLabel("base:", JLabel.TRAILING);
        lblBase.setSize(47, 20);
        lblBase.setLocation(5, 5);
        lblBase.setBackground(Color.white);
        lblBase.setOpaque(true);

        final JTextField txtBase = new JTextField();
        txtBase.setSize(50, 20);
        txtBase.setLocation(60, 5);

        //hight
        JLabel lblHight = new JLabel("hight:", JLabel.TRAILING);
        lblHight.setSize(47, 20);
        lblHight.setLocation(5, 30);
        lblHight.setBackground(Color.white);
        lblHight.setOpaque(true);

        final JTextField txtHight = new JTextField();
        txtHight.setSize(50, 20);
        txtHight.setLocation(60, 30);

        //
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);

        final JLabel lblResult = new JLabel("Triangle base = ? hight = ? area = ? perimeter = ?");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setLocation(0, 100);
        lblResult.setBackground(Color.green);
        lblResult.setOpaque(true);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strBase = txtBase.getText();
                    String strHight = txtHight.getText();
                    double base = Double.parseDouble(strBase);
                    double hight = Double.parseDouble(strHight);

                    Triangle tri = new Triangle(base, hight);
                    lblResult.setText("Triangle base=" + String.format("%.2f", tri.getBase())
                            + "hight=" + String.format("%.2f", tri.getHight())
                            + " area=" + String.format("%.2f", tri.calArea())
                            + " perimeter=" + String.format("%.2f", tri.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtBase.setText("");
                    txtHight.setText("");
                    txtBase.requestFocus();
                }
            }
        });

        frame.add(lblBase);
        frame.add(txtBase);
        frame.add(lblHight);
        frame.add(txtHight);
        frame.add(btnCalculate);
        frame.add(lblResult);

        frame.setVisible(true);
    }

}
